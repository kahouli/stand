<?php

class TwelveFactors 
{
	public function getFactorById($id)	
	{
		$factor_info = array();

		switch ($id){
			case 1:
				$factor_info = array("factor_name" => "Codebase", "factor_description" => "One codebase tracked in revision control, many deploys"); 
			break;
			case 2:
				$factor_info = array("factor_name" => "Dependencies", "factor_description" => "Explicitly declare and isolate dependencies");
			break;
			case 3:
				$factor_info = array("factor_name" => "Config", "factor_description" => "Store config in the environment");
			break;
			case 4:
				$factor_info = array("factor_name" => "Backing services", "factor_description" => "Treat backing services as attached resources");
			break;
			case 5:
				$factor_info = array("factor_name" => "Build, release, run", "factor_description" => "Strictly separate build and run stages");
			break;
			case 6:
				$factor_info = array("factor_name" => "Processes", "factor_description" => "Execute the app as one or more stateless processes");
			break;
			case 7:	
				$factor_info = array("factor_name" => "Port binding", "factor_description" =>"Export services via port binding");
			break;
			case 8:
				$factor_info = array("factor_name" => "Concurrency", "factor_description" =>"Scale out via the process model"  );
			break;
			case 9:
				$factor_info = array("factor_name" => "Disposability", "factor_description" => "Maximize robustness with fast startup and graceful shutdown");
			break;
			case 10:
				$factor_info = array("factor_name" => "Dev/prod parity", "factor_description" =>"Keep development, staging, and production as similar as possible");
			break;
			case 11:
				$factor_info = array("factor_name" => "Logs", "factor_description" => "Treat logs as event streams");
			break;
			case 12:
				$factor_info = array("factor_name" => "Admin processes", "factor_description" => "Run admin/management tasks as one-off processes");
			break;
	 
		}

		return $factor_info;
	}

	function getFactorList()
	{
		$factor_list = array(array("id" => 1, "name" => "I. Codebase"), array("id" => 2, "name" => "II. Dependencies"), array("id" => 3, "name" => "III. Config"), array("id" => 4, "name" => "IV. Backing services"),
                       array("id" => 5, "name" => "V. Build, release, run"), array("id" => 6, "name" => "VI. Processes"), array("id" => 7, "name" => "VII. Port binding"), array("id" => 8, "name" => "VIII. Concurrency"),
					   array("id" => 9, "name" => "IX. Disposability"), array("id" => 10, "name" => "X. Dev/prod parity"), array("id" => 11, "name" => "XI. Logs"), array("id" => 12, "name" => "XII. Admin processes")); 

		return $factor_list;
	}
}